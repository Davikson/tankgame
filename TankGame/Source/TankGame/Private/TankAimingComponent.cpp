// Copyright Davikson

#include "TankGame.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#include "Projectile.h"
#include "TankAimingComponent.h"


// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UTankAimingComponent::BeginPlay()
{
	LastFireTime = FPlatformTime::Seconds();
}

void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (AmmoCount <= 0)
		FiringState = EFiringState::OutOfAmmo;
	else if ((FPlatformTime::Seconds() - LastFireTime) < ReloadTime)
		FiringState = EFiringState::Reloading;
	else if (IsBarrelMoving())
		FiringState = EFiringState::Aiming;
	else
		FiringState = EFiringState::Locked;
}

void UTankAimingComponent::Initialize(UTankBarrel * BarrelToSet, UTankTurret * TurretToSet)
{
	Barrel = BarrelToSet;
	Turret = TurretToSet;
}

void UTankAimingComponent::AimAt(FVector HitLocation)
{
	if (!ensure(Barrel)) return;
	auto StartLocation = Barrel->GetSocketLocation(FName("Muzzle"));
	FVector LaunchVelocity;
	bool bFoundSolution = UGameplayStatics::SuggestProjectileVelocity(this,
		LaunchVelocity,
		StartLocation,
		HitLocation,
		MuzzleVelocity,
		false, 0, 0,
		ESuggestProjVelocityTraceOption::DoNotTrace
	);
	if (bFoundSolution)
	{
		AimDirection = LaunchVelocity.GetSafeNormal();
		MoveBarrelTowards();
	}
}

void UTankAimingComponent::Fire()
{
	if (!ensure(Barrel)) return;
	if (FiringState != EFiringState::Reloading && FiringState != EFiringState::OutOfAmmo)
	{
		if (!ensure(ProjectileBP)) return;
		AProjectile * Projectile = GetWorld()->SpawnActor<AProjectile>(ProjectileBP,
													Barrel->GetSocketLocation(FName("Muzzle")),
													Barrel->GetSocketRotation(FName("Muzzle"))
													);
		Projectile->Launch(MuzzleVelocity);
		LastFireTime = FPlatformTime::Seconds();
		AmmoCount--;
	}
}

void UTankAimingComponent::MoveBarrelTowards()
{
	if (!ensure(Barrel)) return;
	if (!ensure(Turret)) return;
	FRotator BarrelRotator = Barrel->GetForwardVector().Rotation();
	FRotator AimRotator = AimDirection.Rotation();
	FRotator DeltaRotator = AimRotator - BarrelRotator;
	DeltaRotator.Normalize();

	//UE_LOG(LogTemp, Warning, TEXT("%s | %s"), *BarrelRotator.ToString(), *AimRotator.ToString());
	//UE_LOG(LogTemp, Warning, TEXT("%s: %s"), *GetOwner()->GetName(), *DeltaRotator.ToString());
	Barrel->Elevate(DeltaRotator.Pitch);
	Turret->Rotate(DeltaRotator.Yaw);
}

bool UTankAimingComponent::IsBarrelMoving()
{
	if (!ensure(Barrel)) return false;
	return !AimDirection.Equals(Barrel->GetForwardVector(), 0.01f);
}

EFiringState UTankAimingComponent::GetFiringState() const
{
	return FiringState;
}

int32 UTankAimingComponent::GetAmmoCount() const
{
	return AmmoCount;
}
