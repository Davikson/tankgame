// Copyright Davikson

#include "TankGame.h"
#include "TankTurret.h"



void UTankTurret::Rotate(float RelativeSpeed)
{
	RelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -1, 1);
	float RotationChange = RelativeSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	float NewRotation = RelativeRotation.Yaw + RotationChange;
	if (LimitRotation)
		NewRotation = FMath::ClampAngle(NewRotation, LeftRotationLimit, RightRotationLimit);
	SetRelativeRotation(FRotator(0, NewRotation, 0));
}
