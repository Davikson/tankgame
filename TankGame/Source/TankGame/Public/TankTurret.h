// Copyright Davikson

#pragma once

#include "Components/StaticMeshComponent.h"
#include "TankTurret.generated.h"

/**
 * 
 */
UCLASS( ClassGroup = (TankParts), meta = (BlueprintSpawnableComponent) )
class TANKGAME_API UTankTurret : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	void Rotate(float RelativeSpeed);

private:
	UPROPERTY(EditDefaultsOnly, Category = Setup)
	float MaxDegreesPerSecond = 12;

	UPROPERTY(EditDefaultsOnly, Category = Setup)
	bool LimitRotation = false;

	UPROPERTY(EditDefaultsOnly, Category = Setup)
	float LeftRotationLimit = 0;

	UPROPERTY(EditDefaultsOnly, Category = Setup)
	float RightRotationLimit = 0;
};
