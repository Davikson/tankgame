// Copyright Davikson

#pragma once

#include "Components/StaticMeshComponent.h"
#include "TankBarrel.generated.h"

/**
 * 
 */
UCLASS( ClassGroup = (TankParts), meta = (BlueprintSpawnableComponent) )
class TANKGAME_API UTankBarrel : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	void Elevate(float RelativeSpeed);
	
private:
	UPROPERTY(EditDefaultsOnly, Category = Setup)
	float MaxDegreesPerSecond = 4;

	UPROPERTY(EditDefaultsOnly, Category = Setup)
	float MaxElevation = 19;

	UPROPERTY(EditDefaultsOnly, Category = Setup)
	float MinElevation = -9;
};
