// Copyright Davikson

#pragma once

#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"

UENUM()
enum class EFiringState : uint8
{
	Locked,
	Aiming,
	Reloading,
	OutOfAmmo
};

class UTankBarrel;
class UTankTurret;
class AProjectile;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TANKGAME_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialize(UTankBarrel * BarrelToSet, UTankTurret * TurretToSet);

	void AimAt(FVector HitLocation);

	UFUNCTION(BlueprintCallable, Category = "Firing")
	void Fire();

	UFUNCTION(BlueprintCallable, Category = "State")
	EFiringState GetFiringState() const;

	UFUNCTION(BlueprintCallable, Category = "State")
	int32 GetAmmoCount() const;

private:
	// Sets default values for this component's properties
	UTankAimingComponent();

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	void MoveBarrelTowards();

	bool IsBarrelMoving();

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	TSubclassOf<AProjectile> ProjectileBP = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	float MuzzleVelocity = 73000; // in cm/s

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	float ReloadTime = 4; // in s

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	int32 AmmoCount = 10;

	EFiringState FiringState = EFiringState::Reloading;

	float LastFireTime = 0;

	UTankBarrel * Barrel = nullptr;

	UTankTurret * Turret = nullptr;

	FVector AimDirection;
};
